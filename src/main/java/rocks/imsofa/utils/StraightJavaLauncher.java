/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rocks.imsofa.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 *
 * @author lendle
 */
public class StraightJavaLauncher {

    private String javaExecutable = null;
    private String jvmArgs = null;
    private String classpath = null;
    private String mainClass = null;
    private String applicationArgs = null;
    private String errorLogFile = null;
    private String outputLogFile = null;

    public String getErrorLogFile() {
        return errorLogFile;
    }

    public void setErrorLogFile(String errorLogFile) {
        this.errorLogFile = errorLogFile;
    }

    public String getOutputLogFile() {
        return outputLogFile;
    }

    public void setOutputLogFile(String outputLogFile) {
        this.outputLogFile = outputLogFile;
    }

    public String getJavaExecutable() {
        return javaExecutable;
    }

    public void setJavaExecutable(String javaExecutable) {
        this.javaExecutable = javaExecutable;
    }

    public String getJvmArgs() {
        return jvmArgs;
    }

    public void setJvmArgs(String jvmArgs) {
        this.jvmArgs = jvmArgs;
    }

    public String getClasspath() {
        return classpath;
    }

    public void setClasspath(String classpath) {
        this.classpath = classpath;
    }

    public String getMainClass() {
        return mainClass;
    }

    public void setMainClass(String mainClass) {
        this.mainClass = mainClass;
    }

    public String getApplicationArgs() {
        return applicationArgs;
    }

    public void setApplicationArgs(String applicationArgs) {
        this.applicationArgs = applicationArgs;
    }

    public StraightJavaLauncher() {
    }

    public int launch() throws IOException, InterruptedException {
        List<String> cmd = new ArrayList<>();
        if (this.javaExecutable != null) {
            cmd.add(this.javaExecutable);
        }
        if (this.jvmArgs != null) {
            String[] strs = this.jvmArgs.split("\\s+");
            for (String str : strs) {
                cmd.add(str);
            }
        }
        if (this.classpath != null) {
            cmd.add("-classpath");
            cmd.add(this.classpath);
        }
        cmd.add(this.mainClass);
        if(this.applicationArgs!=null){
            String[] strs = this.applicationArgs.split("\\s+");
            for (String str : strs) {
                cmd.add(str);
            }
        }
        ProcessBuilder pb = new ProcessBuilder(cmd);
        System.out.println(pb.command());
        if(this.errorLogFile!=null){
            pb.redirectError(new File(this.errorLogFile));
        }
        if(this.outputLogFile!=null){
            pb.redirectOutput(new File(this.outputLogFile));
        }
        Process p=pb.start();
        int ret=p.waitFor();
        return ret;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        //always load from .launcher.config in the working directory
        File configFile = new File(".launcher.config");
        Properties props = new Properties();
        StraightJavaLauncher launcher = new StraightJavaLauncher();
        try (Reader reader = new BufferedReader(new InputStreamReader(new FileInputStream(configFile), "utf-8"))) {
            props.load(reader);
            String javaExecutable = props.getProperty("javaExecutable");
            String jvmArgs = props.getProperty("jvmArgs");
            String mainClass = props.getProperty("mainClass");
            String applicationArgs = props.getProperty("applicationArgs");
            String errorLogFile = props.getProperty("errorLogFile");
            String outputLogFile = props.getProperty("outputLogFile");

            if (javaExecutable == null) {
                javaExecutable = "java";
            }
            launcher.setJavaExecutable(javaExecutable);
            launcher.setJvmArgs(jvmArgs);
            launcher.setMainClass(mainClass);
            launcher.setApplicationArgs(applicationArgs);
            launcher.setErrorLogFile(errorLogFile);
            launcher.setOutputLogFile(outputLogFile);

            //set classpath to classes/ and all jar files included in libs/
            String classpath = new File(new File("."), "classes").getCanonicalPath();
            File jarsFolder = new File("jars");
            if (jarsFolder.exists() && jarsFolder.isDirectory()) {
                File[] files = jarsFolder.listFiles();
                for (File file : files) {
                    if (file.getName().endsWith(".jar")) {
                        classpath += File.pathSeparator + "jars" + File.separatorChar + file.getName();
                    }
                }
            }

            launcher.setClasspath(classpath);
        }

        System.out.println(launcher.launch());
    }

}
